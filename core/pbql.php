<?php
namespace Core;

class PBQL {

    private $tree;

    public function __construct ( $query = null ) {

        if( $query ) {

            $this->parse($query);
        }
    }

    public function parse ( $query ) {

        try {

            $ast = new \Core\PBQL\ast($query);

            $this->tree = $ast->node;
        } catch ( ParseException $e ) {

            throw $e;
        }
    }

    public function stringify ( $table ) {

        $q = new \Core\pbql\sql($this->tree);

        return $q->getSql($table);
    }
}
