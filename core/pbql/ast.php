<?php
namespace Core\pbql;

// use \Core\pbql\Exception\ParseException;

class ParseException extends \Exception {};

class AST {

    private $query = '';
    private $index = -1;

    public $node;

    private $allowedOperators = array(
        ':' => true,
        '!' => true,
        '<' => true,
        '>' => true,
        '~' => true,
    );

    public function __construct ( $query ) {

        $this->query = $query;
        $this->queryArray = self::splitUTF8String($query);

        try {

            $this->parse();

            // How to throw an exception, try to use custom exections as few as possible.
            // throw new \Core\pbql\Exception\ParseException("Error Processing Request", 1);

        } catch ( ParseExpection $e ) {

            throw $e;
        }
    }

    private function parse () {

        $length = mb_strlen($this->query);

        $this->next();
        $this->skip();

        $node = $this->parseExpression();

        $this->skip();

        if( $this->chr ) {

            throw new ParseException(
                PHP_EOL."Expected end of string instead of `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        $this->node = $node;
    }

    private function next () {

        $this->index++;

        $this->chr = $this->getCharAt($this->index);

        return $this->chr !== '';
    }

    private function getCharAt ( $index ) {

        if( !isset($this->queryArray[$index]) ) {

            return '';
        }

        return $this->queryArray[$index];
    }

    private function skip () {

        while ( $this->chr === ' ' ||  $this->chr === "\n" ||  $this->chr === "\t" ) {

            $this->next();
        }
    }

    static public function splitUTF8String ( $string ) {

        $length = mb_strlen($string);
        $i = 0;
        $result = array();

        for( ; $i < $length; $i++ ) {

            $result[] = mb_substr($string, $i, 1, 'UTF-8');
        }

        return $result;
    }

    private function parseGroup () {

        //
        $this->next();
        $this->skip();

        $node = new \stdClass();
        $node->type = 'Group';
        $node->value = $this->parseExpression();

        $this->skip();

        if( !$this->chr || $this->chr !== ')' ) {

            throw new ParseException(
                PHP_EOL."Group not closed `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        $this->next();
        $this->skip();

        return $node;
    }

    private function parseExpression () {

        $node = new \stdClass();
        $node->type = 'Expression';
        $node->operator = null;    // Order matters

        if( $this->chr === '(' ) {

            $node->left = $this->parseGroup();
        } else {

            $node->left = $this->parseStatement();
        }

        try {

            $node->operator = $this->parseExpressionOperator();
        } catch ( ParseException $e ) {

            return $node->left;
        }


        if( !$node->operator ) {

            return $node->left;
        }

        $node->right = $this->parseExpression();

        return $node;
    }

    private function parseExpressionOperator () {

        $this->skip();

        if( !isset($this->queryArray[$this->index + 1]) ) {

            return null;
        }

        $possibleOperator = $this->chr.$this->queryArray[$this->index + 1];
        $possibleOperator = strtoupper($possibleOperator);

        if( $possibleOperator === 'OR' ) {

            $this->next();
            $this->next();
            $this->skip();

            return $possibleOperator;
        }

        if( !isset($this->queryArray[$this->index + 2]) ) {

            return null;
        }

        $possibleOperator .= $this->queryArray[$this->index + 2];
        $possibleOperator = strtoupper($possibleOperator);

        if( $possibleOperator === 'AND' ) {

            $this->next();
            $this->next();
            $this->next();
            $this->skip();

            return $possibleOperator;
        }

        throw new ParseException(
            PHP_EOL."Expected ExpressionOperator instead of `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
            "{$this->query}".PHP_EOL.
            mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
            );
    }

    // {
    //     "type": "Statement",
    //     "operator": ":",
    //     "left": {
    //         "type": "Identifier",
    //         "value": "id"
    //     },
    //     "right": {
    //         "type": "Value",
    //         "value": 1
    //     }
    // }
    private function parseStatement () {

        $node = new \stdClass();
        $node->type = 'Statement';
        $node->operator = null;    // Order matters

        $node->left = $this->parseVariable();
        $node->operator = $this->parseOperator();
        $node->right = $this->parseValue();

        return $node;
    }

    private function parseVariable () {

        $node = $this->parseIdentifier();

        $i = 0;

        while( $this->chr === '.' ) {

            // echo $i++;

            $this->next();

            // echo "OBJECT";
            $node = (object) array(

                "type" => "Object",
                "object" => $node,
                "property" => $this->parseIdentifier(),
            );
        }

        return $node;
    }

    private function parseIdentifier () {

        $value = '';

        do {

            $chr = $this->chr;

            // Remove,  || $chr === '.'
            if( !(($chr >= 'A' && $chr <= 'z') || ($chr >= '0' && $chr <= '9')) ) {

                break;
            }

            $value .= $chr;

        } while ( $this->next() );

        if( !$value ) {

            throw new ParseException(
                PHP_EOL."Expected Identifier instead of `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        $this->skip();

        return (object) array(

            'type' => 'Identifier',
            'value' => $value
        );
    }

    private function parseOperator () {

        $value = '';

        do {

            $chr = $this->chr;

            if( !isset($this->allowedOperators[$chr]) ) {

                break;
            }

            $value .= $chr;

        } while ( $this->next() );

        if( !$value ) {

            throw new ParseException(
                PHP_EOL."Expected Operator instead of `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        $this->skip();

        return $value;
    }

    private function parseValue () {

        $value = '';
        $quote = '';

        // Are we dealing with a `String literal`?
        if( $this->chr === '"' || $this->chr === '\'' ) {

            $quote = $this->chr;
            $this->next();
        }

        do {

            $chr = $this->chr;

            // Non escaped qoute means end of string
            if( $chr === $quote && $this->queryArray[$this->index - 1] !== '\\' ) {

                break;
            }

            // Escaped qoutes should ignore '\' in output 'm\'s' will be "m's"
            if( $chr === '\\' && $this->queryArray[$this->index + 1] === $quote ) {

                continue;
            }

            // When dealing with (floating) numbers
            if( !$quote && (($chr < '0' || $chr > '9') && $chr !== '.' && $chr !== '-') ) {

                break;
            }

            $value .= $chr;

        } while ( $this->next() );

        if( $quote && $this->chr !== $quote ) {

            throw new ParseException(
                PHP_EOL."Unexpected end `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        if( $value === '' && !$quote ) {

            $node = $this->parseFunctionCall();

            if( $node ) {

                return $node;
            }
        }

        if( ($value === '' && !$quote) ) {

            throw new ParseException(
                PHP_EOL."Expected Value instead of `".mb_substr($this->query, $this->index).'`'.PHP_EOL.
                "{$this->query}".PHP_EOL.
                mb_substr('---------------------------------------------------------------------------------------', 0, $this->index).'^'.PHP_EOL
                );
        }

        if( $quote ) {

            // $value = str_replace('\\'.$quote, $quote, $value);
            $this->next();
        } else {

            $value = 0 + $value;
        }

        return (object) array(

            'type' => 'Value',
            'value' => $value
        );
    }

    private function parseFunctionCall () {

        $node = new \stdClass();
        $node->type = 'Function';
        $node->value = null;

        $value = '';

        do {

            $chr = $this->chr;

            if( $chr === '(' && $this->getCharAt($this->index + 1) === ')' ) {

                $this->next();
                $this->next();
                $node->value = $value.'()';
                return $node;
            }

            if( $chr <= 'A' || $chr >= 'z' ) {

                break;
            }

            $value .= $chr;

        } while ( $this->next() );

        return false;
    }
}
