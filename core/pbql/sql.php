<?php
namespace Core\pbql;

// PBQL only supports SELECT statements
class Sql {

    private $operatorStranslation = [

        ':' => '=',
        '!' => '!',
        '<' => '<',
        '>' => '>',
        '>:' => '>=',
        '<:' => '<=',
        '~' => 'LIKE',
        '!:' => '!=',
        '!<' => '!<',
        '!>' => '!>',
        '!~' => 'NOT LIKE',
    ];

    public function __construct ( /*PBQLAst*/ $ast ) {

        // var_dump($ast);

        $this->ast = $ast;
    }

    public function getSql ( $table ) {

        $this->joins = array();
        $this->table = $table;

        $this->sql = $this->parse($this->ast);
        $joinSql = implode(' ', $this->joins);

        $sql = ($this->sql ? 'WHERE ' : '').$this->sql;

        return "SELECT SQL_CALC_FOUND_ROWS `{$table}`.* FROM `{$table}` {$joinSql} {$sql} LIMIT 0, 100";
    }

    private function parse ( $node ) {

        $sql = '';

        switch ( $node->type ) {

            case 'Group':
                $sql = $this->parseGroup($node);
                break;

            case 'Expression':
                $sql = $this->parseExpression($node);
                break;

            case 'Statement':
                $sql = $this->parseStatement($node);
                break;

            case 'Identifier':
                $sql = '`'.$node->value.'`';
                break;

            case 'Value':
                $sql = '\''.$node->value.'\'';
                break;

            case 'Function':
                $sql = $node->value;
                break;

            /*case 'Object';
                $sql = $this->parseObject($node);
                break;*/

            default:
                throw new \Exception('Unknown node type '.$node->type);

        }

        return $sql;
    }

    private function parseGroup ( $node ) {

        $sql = '(';

        $sql .= $this->parse($node->value);

        $sql .= ')';

        return $sql;
    }

    private function parseExpression ( $node ) {

        $sql = $this->parse($node->left).' '.$node->operator.' '.$this->parse($node->right);

        return $sql;
    }

    private function parseStatement ( $node ) {

        if( $node->left->type === 'Object' ) {

            $this->parseObject($node);
            return;
        }

        $sql = '';

        $sql = $this->parse($node->left).' '.$this->operatorStranslation[$node->operator].' '.$this->parse($node->right);

        return $sql;
    }

    private function parseObject ( $node ) {

        $sql = '';

        $value = $this->parse($node->right);
        $property = $this->parse($node->left->property);

        // var_dump($node->left->object->property);

        $targetTable = $this->parse($node->left->object->property);

        $subnode = $node->left;

        while( isset($subnode->type) && $subnode->type === 'Object' ) {

            // var_dump($subnode);

            if( $subnode->object->type === 'Object' ) {

                 $sql .= "
                    INNER JOIN 
                        `programme`
                    ON
                        (`programme`.`id` = `broadcast`.`programme_id`)
                    ";
            }

           

             // var_dump('---', $subnode);

            // var_dump('!?', $subnode);

             $subnode = $subnode->object;

             // exit;
        }

        // var_dump($subnode);

        // var_dump($sql);

             // var_dump($subnode); exit;

        // Identifier !==

        // var_dump($targetTable, $property, $this->table, $node->left->property); exit;
       
        // $property = $this->parseSubObject($node->left->property);
        

        $sql .= "
            INNER JOIN 
                {$targetTable}
            ON 
                (
                    {$targetTable}.{$property} =`$this->table`.`{$targetTable}_id`
                        AND 
                    {$targetTable}.{$property} {$this->operatorStranslation[$node->operator]} {$value}
                )
        ";

        $this->joins[] = $sql;

        // var_dump($node, $sql);

"SELECT 
    `programme`.* 
FROM 
    `programme` 
INNER JOIN 
    `channel` 
ON 
    (`channel`.`id` = `programme`.`channel_id` AND `channel`.`id` = 1)";

        return $this->parse($node->left->property);
    }

    private function parseSubObject ( $node ) {


    }
}
