<?php
namespace Core;

use Core\Database;
use Core\Collection;

abstract Class Model {

    protected $idAttribute = 'id';

    protected $attributes;

    public function __construct ( $attributes = null ) {

        // $this->db = $db = new \Core\Database\MySQL();

        if( is_object($attributes) ) {

            $this->attributes = (object) $attributes;
        } else {

            $this->attributes = new \stdClass;
        }
    }

    // `&` now we can work with arrays :)
    public function &__get ( $key ) {

        if( isset($this->attributes->{$key}) ) {

            return $this->attributes->{$key};
        }

        $className = '\\'.get_called_class();

        if( isset($className::$definition[$key]) ) {

            $definition = (object) $className::$definition[$key];

            if( $definition->type === 'collection' ) {

                $targetClassName = isset($definition->name) ? $definition->name : $key;
                // $targetDefinition = $targetClassName::$definition;
                $targetRelations = $targetClassName::getRelations();

                foreach ( $targetRelations as $relationDefinition ) {

                    if( $relationDefinition['name'] === $className ) {

                        $targetDefinition = $relationDefinition;
                        break;
                    }
                }

                if( !$targetDefinition ) {

                    echo "Relations not set";
                    $this->attributes->{$key} = null;
                }

                if( $targetDefinition['type'] === 'collection' ) {

                    $modelTable = $className::$table;
                    $targetModelTable = $targetClassName::$table;

                    var_dump($modelTable, $targetModelTable);

                    try {

                        $table = $modelTable.'_'.$targetModelTable;

                        // Koppel tabel, item_audiofile

                        // ast
                        // find relation (if any)
                        // build query, relations should be joined

                        var_dump("{$targetClassName} WHERE {$table}.{$modelTable}_id:{$this->id}");

                        // $this->attributes->{$key} = $targetClassName::find($className::$table."_id:{$this->id}");
                        // Audiofile::find('')
                        $results = $targetClassName::find("{$table}.{$modelTable}_id:{$this->id}");
                        // Now read
                    } catch ( Exception $e ) {

                        $table = $targetModelTable.'_'.$modelTable;

                        // Anders, audiofile_item

                        echo "ASd";
                        // Audiofile::find('')
                        $targetClassName::find("{$table}.{$modelTable}_id:{$this->id}");
                    }

                } else {

                    $this->attributes->{$key} = $targetClassName::find($className::$table."_id:{$this->id}");
                }

                return $this->attributes->{$key};
            }

            if( $definition->type === 'model' ) {

                $targetClassName = isset($definition->name) ? $definition->name : $key;
                $targetAttribute = $targetClassName::$table.'_id';
                $targetId = $this->{$targetAttribute};

                $this->attributes->{$key} = $targetClassName::findOne("id:{$targetId}");

                return $this->attributes->{$key};
            }
        }

        $null = null;

        return $null;
    }

    public function __set ( $key, $value ) {

        $this->attributes->{$key} = $value;
    }

    public function getId () {

        $idAttribute = $this->idAttribute;

        if( isset($this->{$idAttribute}) && $this->{$idAttribute} ) {

            return $this->{$idAttribute};
        }

        return false;
    }

    public function save () {

        $id = $this->getId();

        try {

            if( $id ) {

                $this->update();
            } else {

                $this->create();
            }
        } catch ( Core\Model\SaveException $e ) {

            throw $e;
        }

        return $this;
    }

    public function create () {

        if( !$this->isValid() ) {

            throw new Exception('Validation error');
        }

        $attributes = $this->toJSON();

        /*
        # New object
        Model->save()
            Model->create()
                DB->create()

        # Update object
        Model->save()
            Model->update()
                DB->update()
        */

        DB::create('self::$table', $attributes);
    }
    public function read () {}
    public function update () {}
    public function delete () {}

    /**
     * @param {string} $query pbql query string
     */
    static public function findOne ( $query ) {

        $className = get_called_class();

        $db = new \Core\Database();

        $pbql = new \Core\pbql($query);
        $query = $pbql->stringify($className::$table);

        // echo "Query ".$query.PHP_EOL;

        $result = $db->select($query);

        if( !$result ) {

            throw new \Exception('Entry not found '.$query);
        }

        var_dump($result);

        return new $className($result[0]);
    }

    static public function find ( $query ) {

        $className = get_called_class();

        // We should have a light weight method to check for PBQL string, findOne also accepts primary keys
        // Custom queries would also pre a big plus to support
        // if( isPBQLString($query) ) {
        //
        // }

        $db = new \Core\Database();

        $c = new $className();

        $pbql = new \Core\pbql($query);
        $query = $pbql->stringify($className::$table);

        // echo "Query ".$query.PHP_EOL;

        $results = $db->select($query);

        // var_dump($db->quote("'NOW()'"));
        // foreach ( $db->query('SELECT FOUND_ROWS()') as $row ) {
        //
        //     $length = $row[0];
        // }

        if( !$results ) {

            echo '404';
            return null;
        }

        $r = array();

        foreach ( $results as $result ) {

            $r[] = new $className($result);
        }

        return new Collection($r);
    }

    /**
     * Guess this should be a helper function in a different class? Mhh vs plug-and-play...
     */
    static public function getClassName ( $className ) {

        $parts = explode('\\', $className);

        return $parts[sizeof($parts) - 1];
    }

    /**
     * @return {Array} new array containing relations definition
     */
    static public function getRelations () {

        $className = get_called_class();
        $relations = [];

        foreach ( $className::$definition as $key => $definition ) {

            $type = $definition['type'];

            if( $type === 'collection' || $type === 'model' ) {

                $relations[$key] = $definition;
            }
        }

        return $relations;
    }

    /**
     * Try to find a path from the current model to the target model.
     *
     * @todo path caching
     */
    static public function findPath ( $targetModel, $path = array(), &$match = null ) {

        if( $match && sizeof($path) >= sizeof($match) ) {

            return $match;
        }

        $className = get_called_class();
        $normalizedClassName = self::getClassName($className);

        $path[$normalizedClassName] = $className; //self::getClassName($className);

        $relations = $className::getRelations();

        foreach ( $relations as $key => $definition ) {

            $targetClassName = $definition['name'];
            $normalizedTargetClassName = self::getClassName($targetClassName);

            // Already seen this releation
            if( isset($path[$normalizedTargetClassName]) ) {

                continue;
            }

            // We found our target model
            if( $normalizedTargetClassName === $targetModel ) {

                $path[$normalizedTargetClassName] = $targetClassName;

                // Only add matched path when no path is found, or is shorter then previous matched path
                if( !$match || (sizeof($path) < sizeof($match)) ) {

                    $match = $path;
                }

                return $match;
            }


            $definition['name']::findPath($targetModel, $path, $match);
        }

        return $match;
    }
}
