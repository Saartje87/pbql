<?php
namespace Core;

class Collection implements \Iterator, \Countable, \ArrayAccess {

    private $position = 0;
    private $collection = array();

    public function __construct ( array $collection ) {

        $this->collection = $collection;
    }

    public function first () {}

    public function last () {}

    public function toJSON () {}


    public function  count () {

        return sizeof($this->collection);
    }

    function rewind () {

        $this->position = 0;
    }

    function current () {

        return $this->collection[$this->position];
    }

    function key () {

        return $this->position;
    }

    function next () {

        ++$this->position;
    }

    function valid () {

        return isset($this->collection[$this->position]);
    }

    public function offsetExists ( $index ) {

        return isset($this->collection[$index]);
    }

    public function offsetGet ( $index ) {

        return isset($this->collection[$index]) ? $this->collection[$index] : null;
    }

    public function offsetSet ( $index, $value ) {
        
        $this->collection[$index] = $value;
    }

    public function offsetUnset ( $index ) {
        
        unset($this->collection[$index]);
    }
}
