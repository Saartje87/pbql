<?php
namespace Core;

use \PDO;
use \PDOException;

class Database extends PDO {

	public function __construct () {

        $config = (object) [

            'host' => 'localhost',
            'database' => 'radiobox2',
            'charset' => 'utf8',
            'username' => 'root',
            'password' => 'root',
        ];

		// Requires php 5.3.6+ to make charset work
		parent::__construct("mysql:host={$config->host};dbname={$config->database};charset={$config->charset}", $config->username, $config->password);

		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	public function select ( $sql, array $params = array() ) {

		$stmt = $this->prepare($sql);
		$stmt->execute($params);

		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}

	public function execute ( $sql, array $params = array() ) {

		$stmt = $this->prepare($sql);
		$stmt->execute($params);

		return $stmt->rowCount(); // Affected rows
	}
}
