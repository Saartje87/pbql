<?php
namespace Core;

class Config {

    static protected $config = array();

    /**
     *
     */
    static public function set ( $key, $value ) {

        $ns =& Config::getNamespace($key);
        $ns = $value;
    }

    /**
     *
     */
    static public function get ( $key = '' ) {

        $ns = Config::getNamespace($key);

        return $ns ? $ns : null;
    }

    static private function &getNamespace ( $namespace ) {

        $ns =& Config::$config;
        $parts = explode('.', $namespace);

        while( $part = array_shift($parts) ) {

            if( !isset($ns[$part]) ) {

                $ns[$part] = array();
            }

            $ns =& $ns[$part];
        }

        return $ns;
    }
}
