<?php

// header('Content-Type: application/json');

$start = microtime(true);

include '../../core/autoload.php';
include '../app/autoload.php';

// $path = \App\Models\Item::findPath('Channel');

// $col = \App\Models\Programme::find('channel.id:2');

// var_dump($col); exit;


$pbql = new Core\PBQL('programme.channel.id:1');
// $pbql = new Core\PBQL('channel.name~"3fm"');

// var_dump($pbql);
var_dump($pbql->stringify('broadcast'));

// Model::find('channel.id:1');
// Should use a query like
"SELECT
	`programme`.*
FROM
	`programme`
INNER JOIN
	`channel`
ON
	(`channel`.`id` = `programme`.`channel_id`)";

// echo json_encode($ast->node, JSON_PRETTY_PRINT);

// exit;

// for($i = 10000; $i; $i--) {

	// $path = \App\Models\Item::findPath('Channel');
// }
/*
$item = \App\Models\Item::findOne('id:977');

var_dump($item->audiofile);


$path = \App\Models\Item::findPath('Channel');

if( !$path ) {

	echo('Path not found').PHP_EOL;
} else {

	echo PHP_EOL."Match!".PHP_EOL.PHP_EOL;
	echo "Path including namespaces: ".PHP_EOL.implode('.', $path).PHP_EOL.PHP_EOL;
	echo "Simple path: ".PHP_EOL.implode('.', array_keys($path)).PHP_EOL.PHP_EOL;
}

// var_dump($path);

echo sprintf(PHP_EOL.PHP_EOL."Executed in %f seconds and memory peaked at %f mb",

	microtime(true) - $start,
	memory_get_peak_usage() / 1024 / 1024
);

exit;

*/


$channel = \App\Models\Channel::findOne('name:"npo 3fm"');

if( !$channel ) {

	throw new \Exception('Channel not found');
}

// $channel = \App\Models\Channel::findOne('name:"Radio 2", programmes: name~"a*"');
echo "id: {$channel->id} - '{$channel->name}'".PHP_EOL;


echo "Programme count: ".sizeof($channel->programmes).PHP_EOL;
echo "broadcast count: ".sizeof($channel->programmes[33]->broadcasts).PHP_EOL.PHP_EOL;
// echo "item count: ".sizeof($channel->programmes[33]->broadcasts[0]->items).PHP_EOL;

// var_dump($channel->programmes[33]->broadcasts);

// We should use limited resources.
// Lazy loading and memory management
foreach ( $channel->programmes as $i => $programme ) {

	echo "$i - $programme->id $programme->name ".sizeof($programme->broadcasts).PHP_EOL;

	// if( $i > 0 ) break;
}

// var_dump($channel->programmes);
// var_dump($channel->programmes[100]->broadcasts);






echo sprintf(PHP_EOL.PHP_EOL."Executed in %f seconds and memory peaked at %f mb",

	microtime(true) - $start,
	memory_get_peak_usage(true) / 1024 / 1024
);

// $channels = \App\Models\Channel::find('id>0');
// var_dump($channels);
