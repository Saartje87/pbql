<?php
namespace App\Models;

use Core;

class Item extends Core\Model {

    static protected $table = 'item';

    static protected $definition = array(

        'name' => array('type' => 'string' /* default */, 'length' => 100, 'nullable' => false /* default : which also means its required or not */),
        'broadcast' => array('type' => 'model', 'name' => '\\App\\Models\\Broadcast'),

        // *:*
        'audiofile' => array('type' => 'collection', 'name' => '\\App\\Models\\Audiofile'),
    );

    // Or

    static protected $collections = array();
    static protected $models = array();
}
