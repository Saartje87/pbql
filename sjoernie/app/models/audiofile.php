<?php
namespace App\Models;

use Core;

class Audiofile extends Core\Model {

    static protected $table = 'audiofile';

    static protected $definition = array(

        // *:*
        'items' => array('type' => 'collection', 'name' => '\\App\\Models\\Item'),
    );
}
