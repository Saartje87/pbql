<?php
namespace App\Models;

use Core;

class Broadcast extends Core\Model {

    static protected $table = 'broadcast';

    static protected $definition = array(

        'name' => array('type' => 'string' /* default */, 'length' => 100, 'nullable' => false /* default : which also means its required or not */),
        'programme' => array('type' => 'model', 'name' => '\\App\\Models\\Programme'),
        'items' => array('type' => 'collection', 'name' => '\\App\\Models\\Item'),
        // 'channel' => array('type' => 'model', 'name' => '\\App\\Models\\Channel'),

    );
}
