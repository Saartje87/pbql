<?php
namespace App\Models;

use Core;

class Channel extends Core\Model {

    static protected $table = 'channel';

    // static protected $connection = 'db_connection_2';

    static protected $definition = array(

        'name' => array('type' => 'string' /* default */, 'length' => 100, 'nullable' => false /* default : which also means its required or not */),
        'programmes' => array('type' => 'collection', 'name' => '\\App\\Models\\Programme')
    );


}
