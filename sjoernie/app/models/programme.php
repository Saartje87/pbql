<?php
namespace App\Models;

use Core;

class Programme extends Core\Model {

    static protected $table = 'programme';

    static protected $definition = array(

        'name' => array('type' => 'string' /* default */, 'length' => 100, 'nullable' => false /* default : which also means its required or not */),
        'channel' => array('type' => 'model', 'name' => '\\App\\Models\\Channel'),
        'broadcasts' => array('type' => 'collection', 'name' => '\\App\\Models\\Broadcast')
    );
}
