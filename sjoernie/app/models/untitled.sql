# programme -> 'channel.id:1'
SELECT SQL_CALC_FOUND_ROWS `programme`.* FROM `programme` 
	INNER JOIN 
	    `channel`
	ON 
	    (
	        `channel`.`id` = `programme`.`channel_id`
	            AND 
	        `channel`.`id` = '1'
	    )
	LIMIT 0, 100

# broadcast -> 'programme.channel.id:1'
SELECT SQL_CALC_FOUND_ROWS `broadcast`.* FROM `broadcast`
	INNER JOIN 
	    `programme`
	ON 
	    (
	        `programme`.`id` = `broadcast`.`programme_id`
	    )

	INNER JOIN 
	    `channel`
	ON 
	    (
	        `channel`.`id` = `programme`.`channel_id`
	            AND 
	        `channel`.`id` = '1'
	    )
	LIMIT 0, 100
