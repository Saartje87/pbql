<?php
Core\Config::set('database.default', 'mysql');
Core\Config::set('database.mysql', array(

    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'radiobox2',
    'charset' => 'utf8',
    'username' => 'root',
    'password' => 'root',
));
