# pbql 'orm' framework

## Declare your model
~~~php
<?php
class \App\Models\User extends \Core\Model {

    static protected $table = 'user';

    static protected $definition = [
        // Normal properties
        'name' => (object) ['type' => 'string', 'length' => 100, 'nullable' => true],
        // Relations
        // No more need to declare 1:1, 1:*, *:1, ...
        'role' => (object) ['type' => 'model', 'name' => '\\App\\Models\\Role'],
        'phonenumbers' => (object) ['type' => 'collection', 'name' => '\\App\\Models\\Phonenumber'],
    ]
}
~~~

## Use your model
~~~php
<?php
// Create a new user
$user = new \App\Models\User(array(

    'name' => 'Antonio'
));

// Find single user by `primary id`
$user = \App\Models\User::findOne(1);

// Find single user by `pbql query`
$user = \App\Models\User::findOne('id:1');

// Find all users
$users = \App\Models\User::find();

// Find all users wich starts with letter 'a'
$users = \App\Models\User::find('name~"a*"');
~~~

### Relations

> Relations are lazy loaded.  
> Relation limit of 50 entries.  

~~~php
<?php
$user = \App\Models\User::findOne(1);

var_dump($user->role); // instanceof \App\Models\Role

var_dump($user->phonenumbers); // instanceof \Core\Collection

$publicNumber = $user->phonenumbers->filter(function ( $phonenumber ) {

    return $phonenumber->type === 'public';
});
~~~
