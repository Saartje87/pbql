// https://www.npmjs.org/package/gulp-phpunit

var gulp = require('gulp'),
    notify  = require('gulp-notify'),
    phpunit = require('gulp-phpunit');

gulp.task('phpunit', function() {
    var options = {debug: false, notify: true};
    gulp.src('./tests/**/*.php')
        .pipe(phpunit('phpunit --bootstrap core/autoload.php tests/**/*.php', options))
        .on('error', notify.onError({
            title: "Failed Tests!",
            message: "Error(s) occurred during testing..."
        }));
});

gulp.task('watch', function() {
    gulp.watch('./**/*.php', ['phpunit']);
});

gulp.task('default', ['phpunit', 'watch']);
