<?php

class a {

    public $v = array(

        'foo' => 'bar'
    );
}

$q = new a;
$r = new a;

var_dump($q->v, $r->v);

$q->v[] = 'baz';

var_dump($q->v, $r->v);

exit;
// namespace Soep;

// use Core;

spl_autoload_register(function ( $className ) {

    $path = strtolower($className);
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $path);

    if( !is_readable($path.'.php') ) {

        throw new \Exception($path);
    }

    include $path.'.php';
});

// After autoload

Core\Config::set('database.default', 'mysql');
Core\Config::set('database.mysql', array(

    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'radiobox2',
    'charset' => 'utf8',
    'username' => 'root',
    'password' => 'root',
));

var_dump(Core\Config::get());

class Item extends Core\Model {

    public $table = 'item';

    public function __construct ( $attributes = null ) {

        // $this->manyToOne('broadcast');

        // $this->addManyToOne('');
        // $this->addOneToOne('');
        // $this->addOneToMany('');
        // $this->addManyToMany('');

        parent::__construct($attributes);
    }
}

class Broadcast extends Core\Model {

    public $table = 'broadcast';

    public function __construct ( $attributes = null ) {

        // $this->oneToMany('item', /* Accessible as */ 'items');

        parent::__construct($attributes);
    }

    // Idea
    public function __get ( $key ) {

        if( isset($this->attributes->{$key}) ) {

            return $this->attributes->{$key};
        }

        if( isset($this->relations[$key]) ) {

            // Returns model or collection, depending on relation type
            // when readking collections we should at a limit?

            /* Relation sql example one-to-many; relation with `Item`
            SELECT `item`.* FROM `item`
            WHERE `item`.`broadcast_id` = '{$this->getId()}'
            LIMIT 0, 50

            // broadcast from Item
            SELECT `broadcast`.* FROM `broadcast`
            WHERE `broadcast`.`item_{$this->idAttribute}` = '{$this->broadcast_id}'
            LIMIT 0, 1
            */

            /* Relation sql example many-to-many
            SELECT `item`.* FROM `item`
            INNER JOIN `broadcast_item` ON (`item`.`{$this->idAttribute}` = `broadcast_item`.`item_{$this->idAttribute}`)
            WHERE `broadcast_item`.`broadcast_id` = '{$this->getId()}'
            LIMIT 0, 50
            */
            return $this->attributes->{$key} = $this->readRelation($key);
        }

        return null;
    }
}

$items = Item::find('id<10');

echo "Count: ".sizeof($items).PHP_EOL.PHP_EOL;

foreach ( $items as $item ) {

    echo "{$item->id} {$item->name}".PHP_EOL;
}

// var_dump(sizeof($items), $items);
